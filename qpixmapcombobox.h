#ifndef QPIXMAPCOMBOBOX_H
#define QPIXMAPCOMBOBOX_H

#include <QMenu>
#include <QListWidget>
#include "qpixmapcomboboxitem.h"

class QPixmapComboBox : public QWidget
{
    Q_OBJECT

public:
    QPixmapComboBox(QWidget *parent = nullptr);
    QPixmapComboBoxItem *currentItem() const
    {
        return mCurrentItem;
    }

    void loadImagesFromDirectory(const QString &directory);
    void addItem(QPixmapComboBoxItem *item);

signals:
    void itemActivated(QPixmapComboBoxItem *item);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;

private:
    QPixmapComboBoxItem *mCurrentItem;
    QListWidget *mListWidget;
    QMenu *mMenu;
    bool isImage(const QString &filePath) const;
    void onItemSelected(QPixmapComboBoxItem *item);
};

#endif // QPIXMAPCOMBOBOX_H
