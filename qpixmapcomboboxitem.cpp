#include "qpixmapcomboboxitem.h"
#include <QLabel>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QStylePainter>
#include <QStyleOptionButton>

QPixmapComboBoxItem::QPixmapComboBoxItem(QWidget *parent)
    : QWidget { parent }
{
    mPopUp = new QPixmapPopUp(this);
    connect(mPopUp, &QPixmapPopUp::clicked, this, [&](){
        emit selected(this);
    });
}

QPixmapComboBoxItem::QPixmapComboBoxItem(const QString &text, const QPixmap &pixmap, QWidget *parent)
    : QPixmapComboBoxItem { parent }
{
    setText(text);
    setPixmap(pixmap);
}

void QPixmapComboBoxItem::setPixmap(const QPixmap &pixmap)
{
    mPixmap = pixmap;
    mPopUp->setPixmap(mPixmap.scaled(300, 300));
}

void QPixmapComboBoxItem::mousePressEvent(QMouseEvent *event)
{
    QPoint p = parentWidget()->parentWidget()->parentWidget()->mapToGlobal(event->pos());
    const int x = p.x();
    const int y = p.y();
    const int w = mPopUp->pixmap()->width();
    const int h = mPopUp->pixmap()->height();
    mPopUp->setGeometry(x, y, w, h);
    mPopUp->show();
    QWidget::mousePressEvent(event);
}

void QPixmapComboBoxItem::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawText(2, rect().center().y() + height()/4, mText);
    QWidget::paintEvent(event);
}
