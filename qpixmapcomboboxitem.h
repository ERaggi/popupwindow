#ifndef QPIXMAPCOMBOBOXITEM_H
#define QPIXMAPCOMBOBOXITEM_H

#include <QWidget>
#include <QPixmap>
#include "qpixmappopup.h"

class QPixmapComboBoxItem : public QWidget
{
    Q_OBJECT

public:
    QPixmapComboBoxItem(QWidget *parent = nullptr);
    QPixmapComboBoxItem(const QString &text, const QPixmap &pixmap, QWidget *parent = nullptr);

    void setPixmap(const QPixmap &pixmap);
    QPixmap pixmap() const
    {
        return mPixmap;
    }

    QString text() const
    {
        return mText;
    }

    void setText(const QString &text)
    {
        mText = text;
    }

signals:
    void selected(QPixmapComboBoxItem *item);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;

private:
    QString mText;
    QPixmapPopUp *mPopUp;
    QPixmap mPixmap;
};
#endif // QPIXMAPCOMBOBOXITEM_H
