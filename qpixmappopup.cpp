#include "qpixmappopup.h"
#include <QMouseEvent>
#include <QStyleOptionButton>

QPixmapPopUp::QPixmapPopUp(QWidget *parent)
    : QLabel { parent }
{
    setFrameShape(QLabel::Box);
    setWindowFlag(Qt::Popup);
    setFocusPolicy(Qt::WheelFocus);
    mTimer = new QTimer(this);
    mTimer->setSingleShot(true);
    connect(mTimer, &QTimer::timeout, this, &QLabel::hide);
}

void QPixmapPopUp::show(int msecs)
{
    QLabel::show();
    mTimer->start(msecs);
}

void QPixmapPopUp::mousePressEvent(QMouseEvent *e)
{
    emit clicked();
    hide();
    QLabel::mousePressEvent(e);
}

void QPixmapPopUp::paintEvent(QPaintEvent *)
{
    QStylePainter painter(this);
    QStyleOptionButton option;
    QPalette p(palette());

//    p.setBrush(QPalette::Light, m_currentColor);

//    option.palette = p;
    option.state |= QStyle::State_MouseOver;

}

void QPixmapPopUp::animateHover(bool in)
{
    const QColor &baseColor(palette().brush(QPalette::Light).color());
    const QColor &highlightColor(palette().brush(QPalette::Highlight).color());
    QColor startValue(in ? baseColor : highlightColor);

    if (m_transition) {
        startValue = m_transition->currentValue().value<QColor>();
        m_transition->stop();
    }

    m_transition = new QVariantAnimation(this);

    m_transition->setStartValue(startValue);
    m_transition->setEndValue(in ? highlightColor : baseColor);
    m_transition->setDuration(m_duration);

    connect(m_transition, &QVariantAnimation::valueChanged, [this](const QVariant &value){
        m_currentColor = value.value<QColor>();
        repaint();
    });

    connect(m_transition, &QVariantAnimation::destroyed, [this](){
        m_transition = nullptr;
    });

    m_transition->start(QAbstractAnimation::DeleteWhenStopped);
}
