#ifndef QPIXMAPPOPUP_H
#define QPIXMAPPOPUP_H
#include <QLabel>
#include <QTimer>
#include <QVariantAnimation>
#include <QStyleOptionButton>
#include <QStylePainter>
#include <QEvent>

class QPixmapPopUp : public QLabel
{
    Q_OBJECT
public:
    QPixmapPopUp(QWidget *parent = nullptr);
    void show(int msecs = 3000);

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent *e) override;
    void enterEvent(QEvent *ev) override
    {
        setStyleSheet("QLabel { background-color : blue; }");

    }
    void leaveEvent(QEvent *ev) override
    {
        setStyleSheet("QLabel { background-color : green; }");
    }

    void paintEvent(QPaintEvent *) override;

private:
    QTimer *mTimer;
    void animateHover(bool in);
    QVariantAnimation *m_transition;
    int m_duration;
    QColor m_currentColor;

};

#endif // QPIXMAPPOPUP_H
